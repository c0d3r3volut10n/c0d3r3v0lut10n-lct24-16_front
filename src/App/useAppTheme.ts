import {
  presetGpnDark,
  presetGpnDefault,
  presetGpnDisplay,
} from "@consta/uikit/Theme";
import { ThemeType, setTheme } from "../redux/slices/mainSlice";
import { IconSun } from "@consta/icons/IconSun";
import { IconMoon } from "@consta/icons/IconMoon";
import { IconLightningBolt } from "@consta/icons/IconLightningBolt";
import { useAppDispatch, useAppSelector } from "../redux";

export const useAppTheme = () => {
  const dispatch = useAppDispatch();
  const { theme } = useAppSelector((state) => state.mainState);
  const themes: ThemeType[] = ["Dark", "Default", "Display"];

  const getTheme = (themeString: ThemeType) => {
    switch (themeString) {
      case "Default":
        return presetGpnDefault;
      case "Dark":
        return presetGpnDark;
      case "Display":
        return presetGpnDisplay;
      default:
        return presetGpnDisplay;
    }
  };

  const getItemIcon = (themeString: ThemeType) => {
    switch (themeString) {
      case "Default":
        return IconSun;
      case "Dark":
        return IconMoon;
      case "Display":
        return IconLightningBolt;
      default:
        return IconLightningBolt;
    }
  };

  const setThemeValue = (themeString: ThemeType) => {
    dispatch(setTheme(themeString));
  };

  return {
    getTheme,
    getItemIcon,
    setThemeValue,
    theme,
    themes,
  };
};
