import { memo } from "react";

import "./App.scss";

import { ThemeType } from "../redux/slices/mainSlice";

import "react-toastify/dist/ReactToastify.css";

import { Theme } from "@consta/uikit/Theme";
import { ThemeToggler } from "@consta/uikit/ThemeToggler";
import { TextField } from "@consta/uikit/TextField";
import { Button } from "@consta/uikit/Button";
import { IconAttach } from "@consta/icons/IconAttach";
import { IconSendMessage } from "@consta/icons/IconSendMessage";
import { cnMixScrollBar } from "@consta/uikit/MixScrollBar";
import { Text } from "@consta/uikit/Text";
import { IconUpload } from "@consta/icons/IconUpload";

import styled from "styled-components";
import { useAppTheme } from "./useAppTheme";
import Logo from "../assets/logoCR.svg";
import MessageItemComponent from "./MessageItemComponent";
import { ToastContainer } from "react-toastify";
import { useAppResults } from "./useAppResults";

const App = memo(() => {
  const { getTheme, getItemIcon, setThemeValue, theme, themes } = useAppTheme();

  const {
    rootProps,
    getInputProps,
    isDragActive,
    isLoadingResults,
    isErrorResults,
    chatRef,
    setCurrentPage,
    currentPage,
    resultList,
    inputValue,
    setInputValue,
    onClickAttachFile,
    handleClickSendMessage,
    isLoadingSendMessage,
    isLoadingSendFile,
  } = useAppResults();

  return (
    <Theme preset={getTheme(theme)} className="ThemeTogglerVariants">
      <MainWrapper>
        <Header>
          <img src={Logo} alt="Logo" className="Logo" />
          <ThemeToggler
            items={themes}
            value={theme}
            getItemKey={(item: ThemeType) => item}
            getItemLabel={(item: ThemeType) => item}
            getItemIcon={getItemIcon}
            onChange={setThemeValue}
            direction="downStartLeft"
          />
        </Header>
        <BodyWrapper>
          <Chat {...rootProps} onClick={() => {}}>
            <input {...getInputProps()} />
            {isDragActive && (
              <DragNDropContainer>
                <Text view="primary" size="2xl" weight="bold">
                  Отпустите чтобы отправить файл
                </Text>
              </DragNDropContainer>
            )}
            {isLoadingResults && (
              <DragNDropContainer>
                <Text view="primary" size="2xl" weight="bold">
                  Идет загрузка данных
                </Text>
              </DragNDropContainer>
            )}
            {isErrorResults && (
              <DragNDropContainer>
                <Text view="primary" size="2xl" weight="bold">
                  Ошибка загрузки данных
                </Text>
              </DragNDropContainer>
            )}
            <MessageList className={cnMixScrollBar()} ref={chatRef}>
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                  padding: "10px",
                }}
              >
                <Button
                  view="clear"
                  iconLeft={IconUpload}
                  label="Загрузить ещё"
                  form="round"
                  onClick={() => {
                    setCurrentPage(currentPage + 1);
                  }}
                />
              </div>
              {resultList.length &&
                resultList.map((m, i) => {
                  return <MessageItemComponent m={m} key={i} />;
                })}
            </MessageList>
            <ChatInput>
              <TextField
                placeholder="Введите текст звонка или прикрепите файл"
                value={inputValue}
                onChange={setInputValue}
                disabled={isLoadingSendFile || isLoadingSendMessage}
              />
              <Button
                view="clear"
                onlyIcon
                iconLeft={IconAttach}
                form="round"
                onClick={onClickAttachFile}
                disabled={
                  isLoadingSendFile || isLoadingSendMessage || !!inputValue
                }
                loading={isLoadingSendFile || isLoadingSendMessage}
              />
              <Button
                view="clear"
                onlyIcon
                iconLeft={IconSendMessage}
                disabled={
                  !inputValue || isLoadingSendFile || isLoadingSendMessage
                }
                form="round"
                onClick={handleClickSendMessage}
                loading={isLoadingSendFile || isLoadingSendMessage}
              />
            </ChatInput>
          </Chat>
        </BodyWrapper>
      </MainWrapper>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </Theme>
  );
});

const Header = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 40px;
`;

const MainWrapper = styled.div`
  background-color: var(--color-bg-default);
  width: 100vw;
  height: 100vh;
  max-width: 100vw;
  max-height: 100vh;
  overflow: hidden;
  display: flex;
  flex-direction: column;
`;

const BodyWrapper = styled.div`
  flex-grow: 1;
  padding: 20px;
  max-height: 100%;
`;

const Chat = styled.div`
  max-width: 1000px;
  height: 100%;
  max-height: calc(100% - 70px);
  margin: 0 auto;
  border: 1px solid var(--color-bg-system);
  border-radius: 25px;
  box-shadow: var(--shadow-layer);
  display: flex;
  flex-direction: column;
  overflow: hidden;
  position: relative;
`;

const MessageList = styled.div`
  height: 100%;
  overflow: auto;
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 20px 0;
`;

const ChatInput = styled.div`
  height: 60px;
  padding: 10px 40px;
  display: flex;
  align-items: center;
  gap: 20px;
  border-top: 1px solid var(--color-bg-system);
`;

const DragNDropContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: var(--color-bg-default);
  z-index: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default App;
