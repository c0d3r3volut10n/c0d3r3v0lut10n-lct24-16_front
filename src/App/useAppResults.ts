import { useEffect, useRef, useState, MouseEvent } from "react";
import { useDropzone } from "react-dropzone";
import {
  IResult,
  useApiGetResultsQuery,
  useApiSendFileMutation,
  useApiSendMessageMutation,
} from "../redux/api";
import { toast } from "react-toastify";

export const useAppResults = () => {
  const [resultList, setResultList] = useState<IResult[]>([]);
  const [inputValue, setInputValue] = useState<string | null>(null);
  const [files, setFiles] = useState<File[] | null>(null);
  const [currentPage, setCurrentPage] = useState<number>(1);

  const chatRef = useRef<HTMLDivElement>(null);

  const { getRootProps, getInputProps, acceptedFiles, isDragActive } =
    useDropzone();

  const {
    data: results,
    isLoading: isLoadingResults,
    isError: isErrorResults,
  } = useApiGetResultsQuery([], {
    pollingInterval: 10000,
    skipPollingIfUnfocused: true,
  });

  const [
    apiSendMessage,
    {
      isLoading: isLoadingSendMessage,
      isSuccess: isSuccessSendMessage,
      isError: isErrorSendMessage,
    },
  ] = useApiSendMessageMutation();

  const [
    apiSendFile,
    {
      isLoading: isLoadingSendFile,
      isSuccess: isSuccessSendFile,
      isError: isErrorSendFile,
    },
  ] = useApiSendFileMutation();

  const rootProps = getRootProps();

  const { onClick: onClickAttachFile } = rootProps;

  const [firstTime, setFirstTime] = useState<boolean>(true);

  const handleClickSendMessage = (event: MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    if (!inputValue) return;
    if (!inputValue.trim().length) {
      toast.error("Введите корректное сообщение");
    }
    apiSendMessage({ text: inputValue });
  };

  useEffect(() => {
    if (
      !chatRef ||
      !chatRef.current ||
      !firstTime ||
      !resultList ||
      !resultList.length
    )
      return;
    chatRef.current.scrollTop = chatRef.current.scrollHeight;
    setFirstTime(false);
  }, [chatRef, resultList, firstTime]);

  useEffect(() => {
    if (!results || !results.results.length) return;
    const list = results.results.slice(-10 * currentPage);
    setResultList(list);
  }, [results, currentPage]);

  useEffect(() => {
    if (isLoadingSendMessage) return;
    if (isSuccessSendMessage) {
      toast.success("Сообщение успешно отправленно");
      setInputValue(null);
      return;
    }
    if (isErrorSendMessage) {
      toast.error("Ошибка при отправлении сообщения");
      return;
    }
  }, [isLoadingSendMessage, isSuccessSendMessage, isErrorSendMessage]);

  useEffect(() => {
    if (isLoadingSendFile) return;
    if (isSuccessSendFile) {
      toast.success("Сообщение успешно отправленно");
      setInputValue(null);
      return;
    }
    if (isErrorSendFile) {
      toast.error("Ошибка при отправлении сообщения");
      return;
    }
  }, [isLoadingSendFile, isSuccessSendFile, isErrorSendFile]);

  useEffect(() => {
    if (!acceptedFiles) return;
    if (!acceptedFiles.length) return;
    setFiles(acceptedFiles);
  }, [acceptedFiles]);

  useEffect(() => {
    if (!files) return;
    const formData = new FormData();
    formData.append("file", files[0]);
    apiSendFile(formData);
    setFiles(null);
  }, [files, apiSendFile]);

  return {
    rootProps,
    getInputProps,
    isDragActive,
    isLoadingResults,
    isErrorResults,
    chatRef,
    setCurrentPage,
    currentPage,
    resultList,
    inputValue,
    setInputValue,
    onClickAttachFile,
    isLoadingSendFile,
    handleClickSendMessage,
    isLoadingSendMessage,
  };
};
