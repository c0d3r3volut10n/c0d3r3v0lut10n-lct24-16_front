import { Text } from "@consta/uikit/Text";
import { Avatar } from "@consta/uikit/Avatar";

import LogoAI from "../assets/logoCRChat.svg";

import styled from "styled-components";
import { FC } from "react";
import { IResult } from "../redux/api";

const MessageItemComponent: FC<{ m: IResult }> = ({ m }) => {
  return (
    <MessageItem>
      <Avatar
        name="A I"
        url={LogoAI}
        size="l"
        style={{
          minWidth: "50px",
          minHeight: "50px",
          border: "1px solid var(--color-bg-system)",
        }}
      />
      <MessageItemTextWrapper>
        <MessageItemTextContainer>
          <Text weight="bold" view="primary" style={{ minWidth: "70px" }}>
            Запрос:
          </Text>
          <Text view="secondary">{m.text}</Text>
        </MessageItemTextContainer>
        <MessageItemTextContainer>
          <Text weight="bold" view="primary" style={{ minWidth: "70px" }}>
            Ответ:
          </Text>
          <Text view="secondary">{m.count_discount || "Нет овтета"}</Text>
        </MessageItemTextContainer>
      </MessageItemTextWrapper>
    </MessageItem>
  );
};

const MessageItem = styled.div`
  padding: 10px 30px;
  display: flex;
  align-items: flex-start;
  gap: 20px;
  border: 1px solid var(--color-bg-system);
  margin: 0 20px;
  border-radius: 10px;
`;

const MessageItemTextWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

const MessageItemTextContainer = styled.div`
  display: flex;
  gap: 15px;
`;

export default MessageItemComponent;
