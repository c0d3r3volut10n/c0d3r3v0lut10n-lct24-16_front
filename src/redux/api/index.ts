import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const baseApi = createApi({
  reducerPath: "@@baseApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://api-lct-24-16.c0d3r3v0lut10n.ru/",
  }),
  tagTypes: ["messages"],
  endpoints: () => ({}),
});

export interface IResult {
  id: string;
  text: string;
  discount?: number;
  count_discount?: number;
}

export interface IResponseSendMessage {
  id: string;
  message: string;
  text: string;
}

const defaultApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    apiGetResults: builder.query<{ results: IResult[] }, []>({
      query: () => ({
        url: "results",
      }),
      providesTags: ["messages"],
    }),

    apiSendMessage: builder.mutation<IResponseSendMessage, { text: string }>({
      query: (data) => ({
        url: "message",
        method: "POST",
        body: data,
      }),
      invalidatesTags: ["messages"],
    }),

    apiSendFile: builder.mutation<
      Pick<IResponseSendMessage, "message">,
      FormData
    >({
      query: (data) => ({
        url: "upload",
        method: "POST",
        body: data,
      }),
      invalidatesTags: ["messages"],
    }),
  }),
});

export const {
  useApiGetResultsQuery,
  useApiSendFileMutation,
  useApiSendMessageMutation,
} = defaultApi;
