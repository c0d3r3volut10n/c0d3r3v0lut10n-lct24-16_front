import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export type ThemeType = "Default" | "Dark" | "Display";

interface IMainState {
  theme: ThemeType;
}

const initialState: IMainState = {
  theme: "Dark",
};

export const mainState = createSlice({
  name: "@@main",
  initialState,
  reducers: {
    setTheme: (state: IMainState, action: PayloadAction<ThemeType>) => {
      state.theme = action.payload;
    },
  },
});

export const { setTheme } = mainState.actions;

export const mainReducer = mainState.reducer;
