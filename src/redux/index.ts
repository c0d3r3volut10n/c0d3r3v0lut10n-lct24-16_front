import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import { configureStore } from '@reduxjs/toolkit';
import { baseApi } from './api';
import { mainReducer } from './slices/mainSlice';


export const store = configureStore({
  reducer: {
    mainState: mainReducer,
    [baseApi.reducerPath]: baseApi.reducer,
  },

  devTools: true,

  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({ serializableCheck: false }).concat([
      baseApi.middleware,
    ]),
});

export type RootStateType = ReturnType<typeof store.getState>;
export type AppDispatchType = typeof store.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatchType>();
export const useAppSelector: TypedUseSelectorHook<RootStateType> = useSelector;
