FROM node:latest as build


WORKDIR /app

COPY . /app/

RUN yarn install
RUN yarn lint
RUN yarn build

FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /app/dist /usr/share/nginx/html

EXPOSE 80
ENTRYPOINT [ "nginx", "-g", "daemon off;" ]